<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Appearance</name>
    <message>
        <source>Single-Window layout</source>
        <translation>Disposition en une seule fenêtre</translation>
    </message>
    <message>
        <source>Views are shown in a single window side by side</source>
        <translation>Les affichages sont présentés dans une seule fenêtre, côte à côte</translation>
    </message>
    <message>
        <source>Slim Layout</source>
        <translation>Disposition compacte</translation>
    </message>
    <message>
        <source>Slim layout meant for small screens like in pocket-sized devices.</source>
        <translation>La disposition compacte est conçue pour de petits écrans, comme les portables.</translation>
    </message>
    <message>
        <source>Base Color</source>
        <translation>Couleur de base</translation>
    </message>
    <message>
        <source>Hue</source>
        <translation>Teinte</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <source>Lightness</source>
        <translation>Luminosité</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation>Thème clair</translation>
    </message>
    <message>
        <source>Font size: %1 pt</source>
        <translation>Taille de police : %1 pt</translation>
    </message>
</context>
<context>
    <name>CreateProfile</name>
    <message>
        <source>Enter a Tox alias.</source>
        <translation>Saisissez un alias Tox</translation>
    </message>
    <message>
        <source>Location: %1/%2</source>
        <translation>Emplacement : %1/%2</translation>
    </message>
    <message>
        <source>Enter the profile&apos;s password.</source>
        <translation>Saisissez le mot de passe du profil.</translation>
    </message>
    <message>
        <source>Retype the profile&apos;s password.</source>
        <translation>Resaisissez le mot de passe du profil.</translation>
    </message>
    <message>
        <source>Create Profile</source>
        <translation>Créer le profil</translation>
    </message>
</context>
<context>
    <name>ImportProfiles</name>
    <message>
        <source>&lt;h2&gt;This part of Toxer is work in progress!&lt;/h2&gt;&lt;p&gt;Want to help out?&lt;/p&gt;&lt;p&gt;Visit &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Cette partie de Toxer est un travail en cours !&lt;/h2&gt;&lt;p&gt;Vous voulez aider ?&lt;/p&gt;&lt;p&gt;Allez sur &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; pour plus d'informations.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <source>Name: %1</source>
        <translation>Nom : %1</translation>
    </message>
    <message>
        <source>Status Message: %1</source>
        <translation>Message de statut : %1</translation>
    </message>
    <message>
        <source>Tox-Key:</source>
        <translation>Clé Tox</translation>
    </message>
    <message>
        <source>To Clipboard</source>
        <translation>Dans le presse-papiers</translation>
    </message>
    <message>
        <source>Remove %1 from friends</source>
        <translation>Retirer %1 des amis</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <source>Invite a friend to join your Tox network.</source>
        <translation>Inviter un ou une ami(e) à rejoindre le réseau Tox.</translation>
    </message>
    <message>
        <source>Enter a Tox address.</source>
        <translation>Saisissez une adresse Tox.</translation>
    </message>
    <message>
        <source>Send Tox Invitation</source>
        <translation>Envoyer une invitation Tox</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>MainViewSplit</name>
    <message>
        <source>Copy Tox-Address to Clipboard</source>
        <translation>Copier l'adresse Tox dans le presse-papiers</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <source>Appearance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <source>Appearance settings</source>
        <translation>Paramètres d'apparence</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <source>Tox</source>
        <translation>Tox</translation>
    </message>
    <message>
        <source>Tox network settings</source>
        <translation>Paramètres du réseau Tox</translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <source>Start Profile</source>
        <translation>Lancer le profil</translation>
    </message>
    <message>
        <source>New Profile</source>
        <translation>Nouveau profil</translation>
    </message>
    <message>
        <source>Import Profiles</source>
        <translation>Importer des profils</translation>
    </message>
</context>
<context>
    <name>SelectProfile</name>
    <message>
        <source>Enter the profile&apos;s password</source>
        <translation>Saisissez le mot de passe du profil</translation>
    </message>
    <message>
        <source>Load Profile</source>
        <translation>Charger le profil</translation>
    </message>
</context>
<context>
    <name>Tox</name>
    <message>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <source>Allow IP6 when available.</source>
        <translation>Autoriser l'IP6 lorsque disponible.</translation>
    </message>
    <message>
        <source>Enable UDP for Tox transfer.</source>
        <translation>Activer UDP pour le transfert de Tox.</translation>
    </message>
    <message>
        <source>Proxy Settings</source>
        <translation>Paramètres du proxy</translation>
    </message>
    <message>
        <source>no proxy</source>
        <translation>Aucun proxy</translation>
    </message>
    <message>
        <source>Do not use a proxy.</source>
        <translation>Ne pas utiliser de proxy.</translation>
    </message>
    <message>
        <source>SOCKS proxy for simple socket pipes.</source>
        <translation>Proxy SOCKS pour simplifier la tunnelisation</translation>
    </message>
    <message>
        <source>HTTP proxy using CONNECT.</source>
        <translation>Proxy HTTP utilisant CONNECT.</translation>
    </message>
    <message>
        <source>Proxy Address</source>
        <translation>Adresse du proxy</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <source>Proxy Port</source>
        <translation>Port du proxy</translation>
    </message>
</context>
<context>
    <name>View</name>
    <message>
        <source>Info: File transfer has been cancelled.</source>
        <translation>Information : le transfert du fichier a été annulé.</translation>
    </message>
    <message>
        <source>File transfer not supported by Toxer -&gt; rejecting.</source>
        <translation>Le transfert du fichier n'est pas pris en charge par Toxer -&gt; refus.</translation>
    </message>
    <message>
        <source>Send File</source>
        <translation>Envoyer le fichier</translation>
    </message>
    <message>
        <source>Type a message...</source>
        <translation>Saisissez un message...</translation>
    </message>
    <message>
        <source>Send a file to %1.</source>
        <translation>Envoyer un fichier à %1.</translation>
    </message>
</context>
</TS>
