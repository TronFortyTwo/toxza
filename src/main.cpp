/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2020 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <QGuiApplication>
#include <QQmlContext>
#include <QQuickView>
#include <QScreen>
#include <QThread>
#include <QString>
#include <QTranslator>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

#include "ToxerDesktop.h"

#ifdef SUBDIR_TOXERCORE
#include "ToxerQml.h"
#else
#include "toxercore/ToxerQml.h"
#endif

using Toxer::Api;
using Toxer::Settings;
using ToxerDesktop::UiSettings;

int main(int argc, char *argv[]) {
  QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
  QCoreApplication::setAttribute(Qt::AA_DisableHighDpiScaling);

  QQuickStyle::setStyle(QStringLiteral("Suru"));
  
  QGuiApplication app(argc, argv);
  app.thread()->setObjectName(QStringLiteral("MainThread"));
  //app.setOrganizationName(QStringLiteral("Tox"));
  app.setApplicationName(QStringLiteral("toxza.emanuelesorce"));
  app.setApplicationVersion(QStringLiteral(TOXER_VERSION));

  QTranslator translator;
  { // setup translator
    auto qm_dir = QStringLiteral(":/");
    auto qm_prefix = QStringLiteral("toxza.emanuelesorce");
    auto qm_separator = QStringLiteral("_");
    if (translator.load(QLocale(), qm_prefix, qm_separator, qm_dir)) {
      app.installTranslator(&translator);
    } else {
      qDebug("Looking for translations in: %s", qUtf8Printable(qm_dir));
      qInfo("No translation for locale %s.", qUtf8Printable(QLocale().name()));
    }
  }

  ToxerDesktop::registerQmlTypes();
  Toxer::registerQmlTypes();

  Api toxer;
  //QuickView or whatever
  QQuickView view;

  // TODO: is this needed?
  /*
  QObject::connect(&view, &QQuickView::statusChanged, [&app, &view](QQuickView::Status status) {
    if (status == QQuickView::Ready) {
      QScreen* screen = app.screens()[0];
      view.resize(view.initialSize());
      auto sc = screen->geometry().center();
      view.setPosition(sc.x() - (view.width() / 2), sc.y() - (view.height() / 2));
    }
  });*/

  QObject::connect(&toxer, &Api::profileChanged, [&toxer, &view]() {
    QUrl url;
    url = Toxer::pathTo(QStringLiteral("qml/MainViewSplit.qml"));
    Q_ASSERT(url.isValid());
    auto page = toxer.hasProfile() ? url : Toxer::pathTo(QStringLiteral("qml/Profiles.qml"));

    // important: let the event loop trigger the layout correctly
    //QMetaObject::invokeMethod(&view, "load", Qt::QueuedConnection, Q_ARG(QUrl, page));
	
	if(url == Toxer::pathTo(QStringLiteral("qml/MainViewSplit.qml")))
		QMetaObject::invokeMethod((QObject*)view.rootObject(), "goSplit");
	else
		QMetaObject::invokeMethod((QObject*)view.rootObject(), "goProfiles");
  });

  // TODO:
  //QObject::connect(&view, &QQuickView::quit, &app, &QGuiApplication::quit);
  view.engine()->addImportPath(Toxer::pathTo(QStringLiteral("qml")).toString());

  view.engine()->rootContext()->setContextProperty(QStringLiteral("Toxer"), &toxer);
  view.setResizeMode(QQuickView::SizeRootObjectToView);
  view.setSource(Toxer::pathTo(QStringLiteral("qml/Main.qml")));
  view.show();
  
  return app.exec();
}
