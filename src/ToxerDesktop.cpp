/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2019-2020 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ToxerDesktop.h"
#include "ToxTypes.h"

#include <QClipboard>
#include <QGuiApplication>

/** @ingroup ToxerDesktop
@namespace Toxer
@brief Extends the Toxer namespace for desktop systems.

@fn pathTo
@brief the full file path to an application embedded resource


@namespace ToxerDesktop
@brief Provides helper classes for desktop systems.

@fn registerQmlTypes
@brief Register additional QML data types that are used on desktop systems.

@class Clipboard
@brief Provides access to the desktop system's clipboard.


@class UiSettings
@brief UI settings store
*/

using ToxerDesktop::Clipboard;
using ToxerDesktop::UiSettings;

/** @brief Clipboard constructor */
Clipboard::Clipboard(QObject* parent)
  : QObject(parent)
{
  c_ = acquireClipboard();
}

/**
@brief Get text from clipboard, if any.
*/
QString Clipboard::text() const {
  return c_ ? c_->text() : QString();
}

/**
@brief Copy a text to system clipboard.
*/
void Clipboard::setText(const QString& text) {
  if (c_ && !text.isEmpty()) {
    c_->setText(text);
  }
}

/**
@brief Used to acquire the clipboard controller.
@return The singleton QClipboard instance.

This is used in constructor only and assumes the system clipboard lives at
least as long as this object.
*/
QClipboard* Clipboard::acquireClipboard() {
  auto c = QGuiApplication::clipboard();
  if (!c) {
    qWarning("System clipboard not available or not supported by Qt!");
  }
  return c;
}

UiSettings::UiSettings(QObject* parent)
  : QObject(parent)
  , Settings{ Scope::UserScope, QStringLiteral("ui_settings") }
{
}

/** @brief the applications UI layout */
UiSettings::AppLayout UiSettings::app_layout() const {
  auto v = s_.value(QLatin1String("ui/app_layout"), ToxTypes::enumKey(AppLayout::Split));
  return ToxTypes::enumValue<AppLayout>(v.toByteArray());
}

/** @brief set application UI layout */
void UiSettings::set_app_layout(UiSettings::AppLayout layout) {
  if (set(QLatin1String("ui/app_layout"), ToxTypes::enumKey(layout), ToxTypes::enumKey(app_layout()))) {
    emit app_layout_changed(static_cast<quint8>(layout));
  }
}

/** @brief the applications UI layout */
quint8 UiSettings::app_layout_int() const {
  return static_cast<quint8>(app_layout());
}

void UiSettings::set_app_layout(quint8 layout) {
  set_app_layout(static_cast<AppLayout>(layout));
}

bool UiSettings::fullscreen() const {
  return s_.value(QLatin1String("ui/fullscreen")).toBool();
}

void UiSettings::set_fullscreen(bool enabled) {
  if (set(QLatin1String("ui/fullscreen"), enabled, fullscreen())) {
    emit fullscreen_changed(enabled);
  }
}

QRect UiSettings::geometry() const {
  return s_.value(QLatin1String("ui/geometry")).toRect();
}

void UiSettings::set_geometry(const QRect& rect) {
  if (set(QLatin1String("ui/geometry"), rect, geometry())) {
    emit geometry_changed(rect);
  }
}
