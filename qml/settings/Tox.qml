import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import Toxer.Settings 1.0
import Toxer.Types 1.0

import "../controls" as Controls

Column {
  id: root
  width: 400
  height: 300
  spacing: 10

  ToxSettings { id: tox_settings }
  
  RowLayout {
    Controls.FlatButton { text: qsTr("Reset"); onClicked: tox_settings.resetIp6(); }
    Switch {
      checked: tox_settings.ip6
      onCheckedChanged: tox_settings.ip6 = checked
      text: qsTr("Allow IP6 when available")
      contentItem: Controls.Text {
        leftPadding: parent.indicator.width + 4
        text: parent.text
        verticalAlignment: Text.AlignVCenter
      }
    }
  }

  RowLayout {
    Controls.FlatButton { text: qsTr("Reset"); onClicked: tox_settings.resetUdp(); }
    Switch {
      checked: tox_settings.udp
      onCheckedChanged: tox_settings.udp = checked
      text: qsTr("Enable UDP for Tox transfer")
      contentItem: Controls.Text {
        leftPadding: parent.indicator.width + 4
        text: parent.text
        verticalAlignment: Text.AlignVCenter
      }
    }
  }

  Column {
    spacing: 10
    Controls.Label { text: qsTr("Proxy Settings") }
    RowLayout {
      Controls.FlatButton { text: qsTr("Reset"); onClicked: tox_settings.resetProxyType(); }
      ComboBox {
        width: parent.width
        textRole: "text"
        model: ListModel {
          ListElement { value: ToxTypes.None; text: qsTr("no proxy"); description: qsTr("Do not use a proxy.") }
          ListElement { value: ToxTypes.SOCKS5; text: "SOCKS"; description: qsTr("SOCKS proxy for simple socket pipes.") }
          ListElement { value: ToxTypes.HTTP; text: "HTTP"; description: qsTr("HTTP proxy using CONNECT.") }
        }
        onActivated: {
          var item = model.get(index)
          tox_settings.setProxyType(item.value)
        }
        Component.onCompleted: {
          var v = tox_settings.proxyType
          for(var i = 0; i < count; i++) {
            if (model.get(i).value === v) {
              currentIndex = i
              return
            }
          }
        }
      }
    }
    RowLayout {
      spacing: 4
      Controls.FlatButton { text: qsTr("Reset"); onClicked: tox_settings.resetProxyAddress(); }
      Controls.Label { Layout.alignment: Qt.AlignVCenter; text: qsTr("Proxy Address") }
      Controls.TextField {
        text: tox_settings.proxyAddress;
        onTextChanged: tox_settings.proxyAddress = text
        placeholderText: qsTr("disabled")
        Layout.fillWidth: true;
      }
    }
    RowLayout {
      spacing: 4
      Controls.FlatButton { text: qsTr("Reset"); onClicked: tox_settings.resetProxyPort(); }
      Controls.Label { Layout.alignment: Qt.AlignVCenter; text: qsTr("Proxy Port") }
      SpinBox {
        value: tox_settings.proxyPort
        onValueChanged: tox_settings.proxyPort = value;
        editable: true
        stepSize: 5000
        to: 65535
      }
    }
  }
}
