import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0

//import Ubuntu.Components 1.3 as UC
import Lomiri.Components 1.3 as UC

import Toxer 1.0

import "controls" as Controls
import "style"

UC.Page {
	id: root

	readonly property alias tox: tpq
	property bool online: false

	header: UC.PageHeader {
		id: accountHeader
		title: qsTr("Account")
	}

	Dialog {
		id: edit_dialog
		modal: true

		title: target === targetUsername ? qsTr("Modify username") : qsTr("Modify status message")

		x: 0.5 * (root.width - width)
		y: 0.5 * (root.height - height)

		property alias input: txtfield.text
		property int target: 0
		readonly property int targetStatus: 1
		readonly property int targetUsername: 2

		Column {
			width: txtfield.width
			spacing: units.gu(2)
			Controls.Text {
				width: parent.width
				color: Style.color.nomatch
				visible: edit_dialog.target === edit_dialog.targetUsername
				wrapMode: Text.Wrap
				text: qsTr("Changing username will not rename the profile file")
				maximumLineCount: 3
			}
			Controls.TextField {
				id: txtfield
				width: root.width / 2
			}
		}

		standardButtons: Dialog.Save | Dialog.Cancel

		onAccepted: {
			if(target === targetStatus)
				tpq.setStatusMessage(input)
			else if(target === targetUsername)
				tpq.setUserName(input)
		}
	}

	ToxProfileQuery {
		id: tpq
		onIsOnlineChanged: {
			img_status.source = statusIcon();
			root.online = isOnline()
		}
		onStatusChanged: { img_status.source = statusIcon(); }
		onUserNameChanged: { txt_name.text = "Username: " + userName; }
		onStatusMessageChanged: { txt_status_message.text = "Status Message: " + statusMessage; }

		function statusIcon() {
			return Style.icon.availability(isOnline(), statusInt())
		}
	}

	Column {
		id: layout_tox_self
		anchors {
			margins: units.gu(3)
			top: accountHeader.bottom
			bottom: parent.bottom
			left: parent.left
			right: parent.right
		}
		spacing: units.gu(3)

		Item {
			id: avatarItem
			width: units.gu(15)
			height: width

			Rectangle {
				id: icon_avatar
				height: parent.height
				width: height
				color: Style.color.contrast
				radius: width * 0.5
				UC.Icon {
					source: Style.icon.noAvatar
					height: parent.height
					width: height
					color: "white"
				}
			}
			/*
			Image {
				id: img_avatar
				height: parent.height
				width: height
				fillMode: Image.PreserveAspectFit
				sourceSize: Qt.size(width, height)
				source: Toxer.avatarsUrl() + "/" + tpq.publicKeyStr().toUpperCase() + ".png"
				//source: {
					//var url = Toxer.avatarsUrl() + "/" + tpq.publicKeyStr().toUpperCase() + ".png"
					//return Toxer.exists(url) ? url : Style.icon.noAvatar
				//}
			}*/
			Image {
				id: img_status
				visible: true
				height: Math.max(icon_avatar.height * 0.33, 10)
				width: height
				anchors.bottom: icon_avatar.bottom
				anchors.right: icon_avatar.right
				anchors.rightMargin: -(height / 2)
				fillMode: Image.PreserveAspectFit
				sourceSize: Qt.size(width, height)
				source: tpq.statusIcon()
			}
		}
		Row {
			width: parent.width
			spacing: units.gu(3)
			Controls.Text {
				id: txt_name
				anchors.verticalCenter: parent.verticalCenter
				text: "Username: " + tpq.userName()
			}
			UC.Button {
				id: editName
				anchors.verticalCenter: parent.verticalCenter
				width: units.gu(5)
				iconName: "edit"
				color: Style.color.base
				onClicked: {
					edit_dialog.input = tpq.userName()
					edit_dialog.target = edit_dialog.targetUsername
					edit_dialog.open()
				}
			}
		}
		Row {
			width: parent.width
			spacing: units.gu(3)
			Controls.Text {
				id: txt_status_message
				anchors.verticalCenter: parent.verticalCenter
				text: "Status Message: " + tpq.statusMessage()
			}
			UC.Button {
				id: editStatus
				anchors.verticalCenter: parent.verticalCenter
				width: units.gu(5)
				iconName: "edit"
				color: Style.color.base
				onClicked: {
					edit_dialog.input = tpq.statusMessage()
					edit_dialog.target = edit_dialog.targetStatus
					edit_dialog.open()
				}
			}
		}
		Column {
			width: parent.width
			spacing: units.gu(1)
			Controls.Text {
				width: parent.width
				text: qsTr("Your Tox ID:")
			}
			Controls.TextField {
				width: parent.width
				anchors.horizontalCenter: parent.horizontalCenter
				text: tpq.addressStr().toUpperCase()

				onTextChanged: text = tpq.addressStr().toUpperCase()
			}
			//Controls.RoundButton {
				//text: "Copy to clipboard"
				//onClicked: clipboard.setText(tpq.addressStr().toUpperCase())
			//}
		}
	}
}
