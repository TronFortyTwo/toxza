/*
 * This file is part of the Tox messenger client.
 *
 * Copyright (c) 2021 Emanuele Sorce <emanuele.sorce@hotmail.com>
 *
 * This software is licensed under the terms of the GNU GPLv3 license
 */

//
// This is a database to store:
//	- messages
//	- cached metadata about contacts (name, status) to use when connection is not established yet
//

import QtQuick 2.12
import QtQuick.LocalStorage 2.12

Item {
	
	// Chat database structure:
	//	TABLE NAME contains the public key of the owner of the profile - each profile has its own db
	//
	//	chattype: number- type of chat: e.g. private message, group
	//	chatid: string  - e.g. public key of the person with account has messaged
	//	cfrom:	string  - public key of who sent the message
	//	msgtype: number - type of message (see below)
	//	content: text   - content of the message. e.g. for a text message the text itself
	//	timestamp: timestamp - timestamp of the message
	//	status:	number	- status of the message when applicable (e.g. read/not read not used at the moment)
	//	
	// account, chatid, cfrom and all PKSTR are turned uppercase before being used
	//
	
	// Chat types
	readonly property int chat_type_private: 1	// private message chat
//	readonly property int chat_type_group: 2	// groups still not supported actually
	readonly property int chat_type_unsupported: 3 // none of the above
	
	// Message types
	readonly property int msg_type_textmessage: 1	// Simple text message
	readonly property int msg_type_unsupported: 2	// None of the above
	
	// Statuses
	readonly property int msg_status_not_read: 2		// message is still to be read
	readonly property int msg_status_read: 3			// message has been read
	readonly property int msg_status_unsupported: 1		// no info available
	
	// function UI will use
	function addMessage(account, chattype, chatid, cfrom, msgtype, content, timestamp, status) {
		chatid = chatid.toUpperCase()
		cfrom = cfrom.toUpperCase()
		
		var db = openMessages(account)
		db.transaction(function(tx) {
			tx.executeSql("INSERT INTO msgdb (chattype, chatid, cfrom, msgtype, content, timestamp, status) " + 
				"VALUES (" + chattype + ", '" + chatid + "', '" + cfrom + "', " + msgtype + ", '" + content + "', " + timestamp + ", " + status + ")")
		})
	}
	function getChat(account, chatid) {
		chatid = chatid.toUpperCase()
		var db = openMessages(account)
		var chat = {}
		db.transaction(function(tx) {
			chat = tx.executeSql("SELECT chattype, cfrom, msgtype, content, timestamp, status " +
			"FROM msgdb WHERE chatid='" + chatid + "' ORDER BY timestamp ASC")
		})
		return chat;
	}
	function clearChat(account, chatid) {
		chatid = chatid.toUpperCase()
		var db = openMessages(account)
		db.transaction(function(tx) {
			tx.executeSql("DELETE FROM msgdb WHERE chatid = '" + chatid + "'" )
		})
	}
	
	// Internals
	function openMessages(account) {
		account = account.toUpperCase()
		var db = LocalStorage.openDatabaseSync("messages_" + account, "1.0", "Chat messages", 32000000)
		db.transaction(function(tx) {
			tx.executeSql("CREATE TABLE IF NOT EXISTS msgdb (" +
				"chattype int(255), " +
				"chatid VARCHAR(1024), " +
				"cfrom VARCHAR(1024), " +
				"msgtype int(255), " +
				"content VARCHAR(65535), " +
				"timestamp TIMESTAMP, " +
				"status int(255)" +
			")")
		})
		return db;
	}
	
	//
	// Cache
	// store key/value pairs
	//
	
	function openCache(account) {
		account = account.toUpperCase()
		var db = LocalStorage.openDatabaseSync("cache_" + account, "1.0", "Cached metadata", 100000)
		db.transaction(function(tx) {
			tx.executeSql("CREATE TABLE IF NOT EXISTS data ( key VARCHAR(1024), value VARCHAR(1024) )")
		})
		return db;
	}
	
	function cacheGetKey(account, key) {
		var db = openCache(account)
		var result = undefined
		db.transaction(function(tx) {
			result = tx.executeSql("SELECT value FROM data WHERE key=?", [key])
		})
		if(result.rows.length > 0)
			return result.rows.item(0).value
		else
			return undefined
	}
	function cacheSetKey(account, key, value) {
		var db = openCache(account)
		db.transaction(function(tx) {
			tx.executeSql("INSERT OR REPLACE INTO data(key, value) VALUES(?, ?)", [key, value])
		})
		return true;
	}
}
