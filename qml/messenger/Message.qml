/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 * Copyright (c) 2021 Emanuele Sorce <emanuele.sorce@hotmail.com>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.5 as QC

import Toxer 1.0

import "../controls" as Controls
import "../style"

//RowLayout {
Row {
	id: root

	readonly property bool isMe: pk.toUpperCase() === toxProfile.publicKeyStr().toUpperCase()

	layoutDirection: root.isMe ? Qt.RightToLeft : Qt.LeftToRight
	spacing: units.gu(1)
	leftPadding: units.gu(1)
	rightPadding: units.gu(1)
	
	Item {
		id: pic_container
		width: units.gu(5)
		height: units.gu(5)
		anchors.verticalCenter: parent.verticalCenter
// 		Layout.leftMargin: units.gu(1)
// 		Layout.preferredHeight: units.gu(4)
// 		Layout.preferredWidth: height
// 		Layout.maximumWidth: height
// 		Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft

		Image {
			id: pic
			anchors.fill: parent
			visible: false
			sourceSize: Qt.size(width, height)
			source: {
				var url = Toxer.avatarsUrl() + "/" + pk.toUpperCase() + ".png"
				return Toxer.exists(url) ? url : Style.icon.noAvatar
			}
			asynchronous: true
		}
		Rectangle {
			id: pic_mask
			anchors.fill: pic
			color: "blue"
			radius: width / 2
			clip: true
			visible: false
		}
		OpacityMask {
			anchors.fill: pic_mask
			source: pic
			maskSource: pic_mask
		}
	}
	Row {
		width: parent.width - pic_container.width - time_container.width - units.gu(7)
		layoutDirection: root.isMe ? Qt.RightToLeft : Qt.LeftToRight
		height: msg_container.height
		
		Rectangle {
			id: msg_container
			
			width: Math.min( Math.max(msg_text.implicitWidth, units.gu(12)), parent.width )
			height: msg_text.contentHeight + units.gu(2)
			
// 			Layout.preferredWidth: msg_text.implicitWidth
//			Layout.maximumWidth: parent.width - msg_time.width - pic_container.width - units.gu(7) // 7 to count for margins, spacing
//			Layout.minimumWidth: units.gu(12)
//			Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
//			Layout.minimumHeight: msg_text.contentHeight + units.gu(2)
			color: root.isMe ? Style.color.contrast : "#424954"
			radius: units.dp(5)
			Controls.Text {
				id: msg_text
				padding: units.gu(2)
				text: message
				lineHeight: 1.5
				color: "white" // good contrast with both msg backgrounds
				clip: true
				width: parent.width
				anchors.verticalCenter: parent.verticalCenter
				maximumLineCount: 50
				wrapMode: Text.Wrap
				elide: Text.ElideNone
				horizontalAlignment: root.isMe ? Text.alignLeft : Text.alignRight
			}
		}
	}
	Item {
		id: time_container
		width: units.gu(6)
		height: parent.height
		QC.Label {
			anchors.verticalCenter: parent.verticalCenter
			anchors.left: parent.left
			width: parent.width
			//Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
			//Layout.maximumWidth: contentWidth
			//Layout.minimumWidth: contentWidth
			//Layout.rightMargin: units.gu(1)
			id: msg_time
			
			Component.onCompleted: {
				var msgtime = new Date(ts)
				var today = new Date()
				
				// today
				if(msgtime.getDate() === today.getDate() &&
					msgtime.getMonth() === today.getMonth() &&
					msgtime.getFullYear() === today.getFullYear())
					text = Qt.formatTime(msgtime, "hh:mm")
				
				// other
				else
					text = Qt.formatDate(msgtime, "dd MMM") + "\n" + Qt.formatTime(msgtime, "hh:mm")
			}
		}
	}
}
