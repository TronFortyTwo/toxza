/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
//import Ubuntu.Components 1.3 as UC
import Lomiri.Components 1.3 as UC

import Toxer.Desktop 1.0

import "base" as Base
import "controls" as Controls
import "friend" as Friend
import "style"

UC.Page {
	id: layoutWrapper
	visible: true

	Clipboard { id: clipboard }

	CurrentProfile {
		visible: false
		id: profilePage
	}

	header: UC.PageHeader {
		title: "Toxza"
		subtitle: profilePage.online ? qsTr("Online") : qsTr("Connecting...")
		id: headerItem
		leadingActionBar.actions: [
			UC.Action {
				iconSource: profilePage.online ? Style.icon.online : Style.icon.offline
			}
		]
		trailingActionBar.actions: [
			UC.Action {
				iconName: "info"
				onTriggered: main_container.push("qrc:///qml/About.qml")
				text: qsTr("About")
			},
			UC.Action {
				iconName: "settings"
				onTriggered: main_container.push("qrc:///qml/settings/Overview.qml")
				text: qsTr("Settings")
			},
			UC.Action {
				iconName: "account"
				onTriggered: main_container.push(profilePage)
				text: qsTr("My account")
			},
			UC.Action {
				iconName: "contact-new"
				onTriggered: main_container.push("qrc:///qml/friend/Invite.qml")
				text: qsTr("Add friend")
			}
		]
	}
	Friend.List {
		id: friend_list

		anchors {
			topMargin: units.gu(1)
			top: headerItem.bottom
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}

		Layout.fillWidth: true
		Layout.fillHeight: true
		viewLoader: viewLoader
	}
}
