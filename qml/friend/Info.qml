import QtQuick 2.12
import QtQuick.Layouts 1.12
//import Ubuntu.Components 1.3
import Lomiri.Components 1.3

import Toxer 1.0
import Toxer.Desktop 1.0

import "../base" as Base
import "../controls" as Controls
import "../style"

Base.Page {

	header: PageHeader {
		id: headerItem
		title: qsTr("Info")
	}

	id: root

	property int friendNo: -1

	ToxFriends { id: friends_manager }
	ToxProfileQuery { id: myself }

	Column {
		id: col
		anchors.fill: parent
		anchors.topMargin: headerItem.height + units.gu(1)
		anchors.leftMargin: units.gu(2)
		anchors.rightMargin: units.gu(2)
		spacing: units.gu(3)

		Image {
			anchors.horizontalCenter: parent.horizontalCenter
			sourceSize: Qt.size(root.height / 5, root.height / 5)
			source: {
				var url = Toxer.avatarsUrl() + "/" +
					friends_manager.publicKeyStr(friendNo).toUpperCase() + ".png"
					return Toxer.exists(url) ? url : Style.icon.noAvatar
			}
		}
		Controls.Text {
			anchors.horizontalCenter: parent.horizontalCenter
			width: parent.width
			text: qsTr("Name: %1").arg(friends_manager.name(friendNo))
		}
		Controls.Text {
			anchors.horizontalCenter: parent.horizontalCenter
			width: parent.width
			text: qsTr("Status Message: %1").arg(friends_manager.statusMessage(friendNo))
		}
		Controls.Text {
			anchors.horizontalCenter: parent.horizontalCenter
			width: parent.width
			text: qsTr("Tox-Key:")
		}
		Controls.TextField {
			anchors.horizontalCenter: parent.horizontalCenter
			width: parent.width
			text: friends_manager.publicKeyStr(friendNo).toUpperCase()
			onTextChanged: text = friends_manager.publicKeyStr(friendNo).toUpperCase()
		}
		Controls.FlatButton {
			Layout.alignment: Qt.AlignCenter
			text: qsTr("Clear chat history")
			onClicked: {
				chatDatabase.clearChat( myself.publicKeyStr().toUpperCase(), friends_manager.publicKeyStr(friendNo).toUpperCase() )
			}
		}
		Controls.FlatButton {
			Layout.alignment: Qt.AlignCenter
			text: qsTr("Remove %1 from friends").arg(friends_manager.name(friendNo))
			onClicked: { friends_manager.remove(friendNo) }
		}
	}
}
