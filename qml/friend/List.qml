/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
//import Ubuntu.Components 1.3 as UC
import Lomiri.Components 1.3 as UC

import Toxer 1.0

import "../base" as Base
import "../controls" as Controls
import "../style"
import "../messenger" as Messenger

Base.View {
  id : root

	ToxFriends {
		id: friends_manager
	}

	ToxProfileQuery {
		id: myself
	}

	// File transfer - ignore everything for now
	/*Connections {
		target: Toxer
		onFileCancelled: {
			for(var fri in friendsList.contentItem.children) {
				var fr = friendsList.contentItem.children[fr]
				if(fr.my_friend.fno == friendNo) {
					fr.friend_view.addMessageItem(myself.publicKeyStr().toUpperCase(), qsTr("Info: File transfer has been cancelled."));
				}
			}
		}
		onFileRequest: {
			for(var fri in friendsList.contentItem.children) {
				var fr = friendsList.contentItem.children[fr]
				if(fr.my_friend.fno == friendNo) {
					fr.friend_view.addMessageItem(myself.publicKeyStr().toUpperCase(), qsTr("File transfer not supported by Toxza -> rejecting."));
					fr.my_friend.cancelFile(fileNo)
				}
			}
		}
	}*/

	ListView {
		id: friendsList
		anchors.fill: parent
		model: friends_manager.list.slice(0)
		clip: true
		focus: true
		delegate: Delegate {

			property alias my_friend: this_friend
			property url my_friend_icon: this_friend.statusIcon()
			property alias friend_view: messengerView

			id: friendDelegate
			width: friendsList.width
			height: units.gu(7)

			avatar.source: {
				var url = Toxer.avatarsUrl() + "/" +
					my_friend.pkStr().toUpperCase() + ".png"
				return Toxer.exists(url) ? url : Style.icon.noAvatar
			}
			statusLight.source: my_friend_icon

			Component.onCompleted: {
				username = my_friend.name
				status_message = my_friend.statusMessage
				pk = my_friend.pkStr().toUpperCase()
			}

			Messenger.View {
				id: messengerView
				visible: false
				friendNo: modelData
			}

			onClicked: {
				main_container.push(messengerView)
				unreadMessages = 0
			}

			ToxFriend {
				id: this_friend
				fno: modelData

				onMessage: {
					messengerView.addMessageItem(pkStr().toUpperCase(), message)
				}
				onIsOnlineChanged: {
					my_friend_icon = statusIcon()
				}
				onAvailabilityChanged: {
					my_friend_icon = statusIcon()
				}
				onNameChanged: {
					username = name
					// save changes for next time
					myself.persist()
				}
				onStatusMessageChanged: {
					status_message = statusMessage
					// save changes for next time
					myself.persist()
				}

				function statusIcon() {
					return Style.icon.availability(isOnline, availability)
				}
			}

			leadingActions: UC.ListItemActions {
				actions: [
					UC.Action {
						iconName: "delete"
						onTriggered: {
							friends_manager.remove(modelData);
						}
					}
				]
			}
		}
	}
}
