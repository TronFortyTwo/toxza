/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
//import Ubuntu.Components 1.3 as UC
import Lomiri.Components 1.3 as UC

import "controls" as Controls
import "style"

ColumnLayout {
	id: root
	anchors.leftMargin: units.gu(2)
	anchors.rightMargin: units.gu(2)
	spacing: units.gu(2)

	function profileName() {
		return txtToxAlias.text.replace(new RegExp("\\W", "gm"), "_")
	}

	Controls.TextField {
		id: txtToxAlias
		Layout.fillWidth: true
		Layout.minimumHeight: 34
		placeholderText: qsTr("Enter a Tox alias.")
	}
	Text {
		id: txtProfilePath
		Layout.fillWidth: true
		Layout.leftMargin: 5
		Layout.rightMargin: 5
		Layout.bottomMargin: 15
		color: Style.color.text
		wrapMode: Text.WrapAnywhere
		text: {
			var path = qsTr("Location: %1/%2")
				.arg(Toxer.profileLocation())
				.arg(root.profileName())
			if (txtToxAlias.text) {
				path += ".tox"
			}
			return path
		}
	}
	Controls.TextField {
		id: txtPassword
		Layout.fillWidth: true
		Layout.minimumHeight: 34
		echoMode: TextInput.Password
		placeholderText: qsTr("Enter the profile's password.")
	}
	Controls.TextField {
		id: txtRepeatPassword
		Layout.fillWidth: true
		Layout.minimumHeight: 34
		condition: txtRepeatPassword.text === txtPassword.text
		echoMode: TextInput.Password
		placeholderText: qsTr("Retype the profile's password.")
	}

	UC.ActivityIndicator {
		id: profile_loading
		Layout.minimumWidth: parent.width/5
		height: width
		anchors.margins: units.gu(3)
		running: true
		visible: false
	}

	Controls.FlatButton {
		id: btnCreate
		Layout.fillWidth: true

		enabled: txtToxAlias.length > 0 && txtPassword.length > 0 &&
			txtRepeatPassword.text === txtPassword.text
		padding: units.gu(3)
		text: qsTr("Create Profile")

		onClicked: {
			var pname = root.profileName()
			var pw = txtPassword.text
			var alias = txtToxAlias.text

			txtPassword.text = ""
			txtRepeatPassword.text = ""
			txtToxAlias.text = ""
			if (Toxer.createProfile(pname, pw, alias)) {
				creation_dialog.title = qsTr("Profile created")
				creation_dialog.open()
			}
			else {
				creation_dialog.title = qsTr("Profile creation failed!")
				creation_dialog.text = qsTr("Make sure a profile with that name already exists or check logs for details")
				creation_dialog.open()
			}
		}
	}

	Dialog {
		id: creation_dialog

		x: (parent.width - width)/2
		y: (parent.height - height)/2

		width: parent.width * 0.6
		height: parent.height * 0.8

		dim: true

		property alias text: dialog_label.text

		Label {
			id: dialog_label
			width: parent.width
			wrapMode: Text.Wrap
		}

		standardButtons: Dialog.Ok
	}
}
