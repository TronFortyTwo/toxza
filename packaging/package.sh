#!/bin/sh
set -e

SELF=$(dirname `readlink -f $0`)
SNAP_DOCKER_IMAGE=snapcore/snapcraft
P=$SELF/..

die(){
  echo -e "\n\033[1;31mError: $1\033[m"
  exit 1
}

execute() {
  local b=$(which $1)
  if [ -z "$b" ]; then die "No $1 binary found. Is it installed?"; fi
  $b "${@:2}"
}

create_flatpak(){
  execute flatpak-builder \
    --force-clean \
    $SELF/flatpak-build \
    $SELF/flatpak.json
}

snap_docker_do(){
  local p_mapped=/w
  # update snapcraft image
  execute "docker" \
    run --rm \
    -v "$P":"$p_mapped" \
    -w $p_mapped/packaging \
    $SNAP_DOCKER_IMAGE \
    "$@"
}
snap_docker(){
  echo -e "Updating $SNAP_DOCKER_IMAGE image"
  execute "docker" pull $SNAP_DOCKER_IMAGE
  echo "DONE"

  echo "Building Toxer package"
  snap_docker_do bash -c "apt update -qq && snapcraft"
}

# build snap
CMD="${1:-flatpak}"
case $CMD in
  snap-docker) snap_docker ;;
  snap-docker-do) snap_docker_do "${@:2}" ;;
  flatpak) create_flatpak ;;
  *) die "$0 needs a command." ;;
esac

echo "Done, bye!"
