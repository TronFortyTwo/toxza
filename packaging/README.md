# Create Desktop Packages for the "Toxer" Application

To make packaging Toxer simple the "build-package.sh" tool is provided that helps you along the way.

# Linux
## Flatpak (default)

Install Flatpak on your system and the SDK/Platform basis:

```bash
QT_VERSION='5.12' \
  flatpak install org.kde.Sdk//$QT_VERSION \
  flatpak install org.kde.Platform//$QT_VERSION
```

Then run:

```bash
./package.sh flatpak
```

## Ubuntu-Snap

**Note: Packaging Toxer with Ubuntu-Snap is a work in progress! Feel free to help out on this end!**

Install Ubuntu-Snap and Docker on your System.

### Why Docker?
While Docker uses (mostly temporary) storage resources it is also a non-destructive - and currently the only - way to create Snaps on Systems other than Ubuntu. Even with Ubuntu this has the advantage of not polluting your System with "apt" packages throughout the build.

### Run the Build-Package Tool

```bash
./build-package.sh snap-docker
```

Manual commands can be run in the prepared Docker container manually:

```bash
./build-package.sh snap-docker-do echo "Hello World"
```

# Mac
Not available yet. Feel free to help out on this end!

# Windows
Not available yet. Feel free to help out on this end!
